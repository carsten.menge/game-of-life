const colorDead  = "#000000"
const colorAlive = "#ffffff"
const cSize = 5;

var running = false;
var interval;

class Field {
    height;
    width;
    factor;
    cells = [];
    constructor(h, w, f) {
        this.height = h;
        this.width = w;
        this.factor = f;
        this.initArray()
    }

    initArray() {
        for (let x = 0; x < this.width; x++) {
            this.cells[x] = [];
            for (let y = 0; y < this.height; y++) {
                let c = 0;
                if (Math.random() < this.factor) {
                    c = 1
                }
                this.cells[x][y] = c;
            }
        }
    }

    nextGeneration() {
        let tempCells = _.cloneDeep(this.cells);
        for (let x = 0; x < this.width; x++) {
            for (let y = 0; y < this.height; y++) {                
                if (this.cells[x][y] === 1 && this.countLivingNeighbours(x, y) > 3) {
                    tempCells[x][y] = 0;
                } else if (this.cells[x][y] === 1 && this.countLivingNeighbours(x, y) < 2) {
                    tempCells[x][y] = 0;
                } else if (this.cells[x][y] === 0 && this.countLivingNeighbours(x, y) === 3) {
                    tempCells[x][y] = 1;
                }
            }
        }
        this.cells = _.cloneDeep(tempCells);
    }

    countLivingNeighbours(x, y) {
        let c = 0;

        // column left of origin
        c += this.cellAliveHelper(x-1, y-1) ? 1 : 0
        c += this.cellAliveHelper(x-1, y) ? 1 : 0
        c += this.cellAliveHelper(x-1, y+1) ? 1 : 0

        // column around origin
        c += this.cellAliveHelper(x, y-1) ? 1 : 0
        c += this.cellAliveHelper(x, y+1) ? 1 : 0

        // column right of origin
        c += this.cellAliveHelper(x+1, y-1) ? 1 : 0
        c += this.cellAliveHelper(x+1, y) ? 1 : 0
        c += this.cellAliveHelper(x+1, y+1) ? 1 : 0

        return c;
    }

    cellAliveHelper(x, y) {
        try {
            return this.cells[x][y];
        } catch {
            return 0
        }
    }
}

function drawField(field) {
    let canvas = document.getElementById("canvas");
    canvas.width = field.width * cSize;
    canvas.height = field.height * cSize;
    let ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    for (let x = 0; x < field.width; x++) {
        for (let y = 0; y < field.height; y++) { 
            let color = field.cells[x][y] == 1 ? colorAlive : colorDead;
            ctx.fillStyle = color;
            ctx.fillRect(x * cSize, y * cSize, cSize, cSize)
        }
    }
}

var field = new Field(100, 100, 0.5);
drawField(field);

document.getElementById("new").addEventListener("click", function() {
    let h = document.getElementById("height").value;
    let w = document.getElementById("width").value;
    let f = document.getElementById("factor").value;
    field = new Field(h, w, f);
    drawField(field);
});

document.getElementById("next").addEventListener("click", function() {
    field.nextGeneration();
    drawField(field);
});

document.getElementById("canvas").addEventListener("click", function(e) {
    let rect = this.getBoundingClientRect();
    let x = Math.floor((e.clientX - rect.left) / cSize);
    let y = Math.floor((e.clientY - rect.top) / cSize);
    
    field.cells[x][y] = field.cells[x][y] == 0 ? 1 : 0;
    drawField(field);
});

document.getElementById("canvas").addEventListener("contextmenu", function(e) {
    let rect = this.getBoundingClientRect();
    let x = Math.floor((e.clientX - rect.left) / cSize);
    let y = Math.floor((e.clientY - rect.top) / cSize);
    
    console.log(field.countLivingNeighbours(x, y));
});

document.getElementById("start").addEventListener("click", function(){
    let speed = document.getElementById("speed").value;

    if (running) {
        this.innerText = "Run Simulation";
        clearInterval(interval);
        running = false;
    } else {
        this.innerText = "Stop Simulation";
        interval = setInterval(() => {
            field.nextGeneration();
            drawField(field);
        }, speed);
        running = true;
    }
});